import sys

error = False

x = 1

warehouse = {}
overview = []
account = 0
total = 0
commands = ["saldo", "zakup", "sprzedaż", "konto", "magazyn", "przeglad"]

arguments = sys.argv[1:]
print(arguments)

while True:
    operation = arguments.pop(0)

    if operation not in commands:
        error = True
        print("Error")
        break

    elif operation == "saldo":
        balance_amount = float(arguments.pop(0))
        balance_comment = str(arguments.pop(0))
        account = account + balance_amount
        overview.append(balance_amount)
        overview.append(balance_comment)
        print("{}: {} {}".format(operation, balance_amount, balance_comment))

    elif operation == "zakup":
        o_purchase = arguments.pop(0)
        purchase_price = float(arguments.pop(0))
        purchase_qt = int(arguments.pop(0))

        if account - (purchase_qt * purchase_price) < 0:
            print("Niewystrczające srodki")
            error = True
            break

        total = purchase_price * purchase_qt
        account -= total
        warehouse.setdefault(o_purchase, 0)
        warehouse[o_purchase] += purchase_qt
        overview.append(operation)
        overview.append(o_purchase)
        overview.append(purchase_qt)
        overview.append(purchase_price)
        print("magazyn", warehouse)
        print(overview)

    elif operation == "sprzedaż":
        o_sales = arguments.pop(0)
        sales_price = float(arguments.pop(0))
        sales_qt = int(arguments.pop(0))
        if sales_price < 0:
            print("Cena nie może być niższa niż 0!")
            error = True
            break
        if sales_qt < 0:
            print("Ilosc sprzedanych sztuk musi byc wieksza niz 0!")
            error = True
            break
        if warehouse.get(o_sales, 0) - sales_qt < 0:
            print("Niewystarczajaca ilosc przedmiotow w magazynie!")
            error = True
            break
        total = sales_qt * sales_price
        account += total
        warehouse[o_sales] -= sales_qt
        overview.append(operation)
        overview.append(o_sales)
        overview.append(sales_qt)
        overview.append(sales_price)

    elif operation == "konto":
        print(account)

    elif operation == "magazyn":
        print(warehouse)

    elif operation == "przeglad":
        print(overview)

    elif operation == "stop":
        break

    else:
        print("Blad")
        break

print("Saldo po operacjach: ", account)
print("Stan magazynu: ", warehouse)
print("Zmiany: ", overview[:])


